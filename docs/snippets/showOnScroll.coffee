    mounted: -> window.addEventListener 'scroll', @onScroll
    beforeDestroy: -> window.removeEventListener 'scroll', @onScroll
    data: ->
      showMenu: true
      lastScroll: 0
    methods:
      onScroll: ->
        currentScroll = window.pageYOffset or document.documentElement.scrollTop
        if currentScroll >= 0
          @showMenu = currentScroll < @lastScroll
          @lastScroll = currentScroll