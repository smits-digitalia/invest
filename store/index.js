import {parser} from '~/code/parser';

export const state = () => ({
  oglasi: [],
	oglas: ''
});

export const mutations = {
  setOglasi (state, oglasi) { state.oglasi = oglasi },
	setOglas  (state, oglas)  { state.oglas  = oglas  }
};

export const actions = {
  async nuxtServerInit ({commit}) {
    let files = await require.context('~/file/data/', true, /\.md$/);
    let datas = files.keys().map(path => parser( files(path).default, path.slice(2,-3) ));
    await commit('setOglasi', datas);
	}
};