# Dokumentacija

- [Dnevnik projekta - Trello](https://trello.com/invite/b/uP3HofXP/2226f4c5e7c542b8b077c0ee4b0803d3/web-invest-nekretnine)

## Osnovne informacije

- Naručitelj: Invest Nekretnine d.o.o., Goran i Nikolina Radosović
- Početak rada: 9.3.2020., Uvod u posao: 2.3.
- Stranica za nekretninu završena 11.3
- Naslovnica predviđeni završetak: 15.3.
- Izvođač: Smits Digitalia, Nikola Smoljanovic nikola@smits.dev