1
Glavni opisni naslov, prvi i glavni tekst, preletit će ga otprilike 70% ljudi
- Tip nekretnine: Stan
- Stambena površina: 55.3 m2
- Lokacija: Zagreb Centar
- Cijena: 64 tis. eur
- Cijena po m2: 1.157 eur

1
Opisni naslov, dva puna reda minimalno, tj 60ak znakova
- Na ovu listu idu čvrste informacije, činjenice. Doslovno natuknice.
- Sve te činjenice podržavaju ono što piše u naslovu. Zajedno trebaju stvarati sliku
- Specifikacije pogodnosti
- Zadnja stvar na listi jako upada u oči

5 6

4
Opis je benefit, opis nije činjenica. Mogu biti i malo dulji - idealno tri reda, tj cca 100 znakova.
- Činjenice su brojevi, kategorizacije i slično. Činjenice se prelete, ne čitaju se
- Opis ne daje činjenice nego ih povezuje u neku poantu, daje im zajednički smisao
- Koliko činjenica je proizvoljno - lista od 3 do 6 i svaka natuknica ne preko 3 reda
- Kratke su uočljive, duge su zamorne

5
Opisni naslov, dva puna reda minimalno, tj 60ak znakova
- Specfikacije pogodnosti
- Na ovu listu idu čvrste informacije, činjenice. Doslovno natuknice.
- Sve te činjenice podržavaju ono što piše u naslovu. Zajedno trebaju stvarati sliku
- Zadnja stvar na listi jako upada u oči

7 6 8 4

---

Zagreb%204s%20112%20esplanada

# Izvana

0 izvana_bbjxj4.jpg
Sve slike bi trebale imati neki opis - može bez opisa ali vrlo rijetko

# Kuhinja

1 kuhinja-prozor_nwk3oc.jpg
Opis ispod slike, ovo će pročitati 60% ljudi, treba bit sočan

2 kuhinja_xlc4mk.jpg
Kuhinja je odmah zdesna. Renovirana je ove godine

# Dnevni

3 dnevni-vrata_gn4ugd.jpg
Najbolje u jedan red ako uspijete

4 126549182_ukpdgg.jpg
Ispod slika treba doći opis, ali može bit i činjenica ako nemate inspiracije

5 dnevni-vrata-koso_swbaka.jpg
Opis ispod slike će pročitati 60% ljudi dok će tekst u paragrafima pročitati samo 10% ljudi

6 dnevni-1_fssvcu.jpg
Opis ispod neki drugačiji (može i činjenica ako nemate ideja)

# Hodnik

7 hodnik-ravno_ckln8i.jpg
Velika dnevna soba je ravno s ulaznih vrata

8 hodnik-niz_bqpeqs.jpg
Opis ispod neki drugačiji (može i činjenica ako nemate ideja)