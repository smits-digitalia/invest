import {parser} from './code/parser'

load =
	raw:
		test: /\.(txt|md)$/i
		loader: 'raw-loader'

export default
 
	dir:
		assets:     'file'
		components: 'use'
		layouts:    'layout'
		pages:      'page'
	head:
		title: process.env.npm_package_name or ''
		meta: [
			{ charset: 'utf-8' }
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' }
			{ hid: 'description', name: 'description', content: process.env.npm_package_description or '' }
			]
		link: [ { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' } ]
		script: []

	buildModules: [] # dev-modules              
	modules: [ # Nuxt modules
		'nuxt-coffeescript-module'
		'@nuxtjs/svg'
		'@nuxtjs/style-resources'
		]
	plugins: [] # Plugins loaded before mounting the App
	
	css: [ # Global CSS
		'~/file/style/base.styl'
		'~/file/style/section.styl'
		'~/file/style/button.styl'
		]
	styleResources: stylus: '~/file/style/use/*.styl'
	loading: color: '#398bee' # Progress-bar color
	
	router:
		linkActiveClass: 'active'
		linkExactActiveClass: 'exact'
		linkPrefetchedClass: 'eager'

	mode: 'universal' # SSR + SPA

	generate:
		routes: ->
			fs = require 'fs'
			path = require 'path'
			fs.readdirSync('./file/data').map (file) =>
				# got = await require("./file/data/prodaja/#{file}")
				# payload: parser got.default, name.slice(1,-3)
				route: "/#{path.parse(file).name}"

	build: `{
	  extend (config, { isDev, isClient }) {
	    config.module.rules.push(load.raw);
	    if (isDev) { config.mode = 'development' };
	    if (isClient) { config.devtool = 'source-map' };
	}}`

	server:
		port: 3000
		host: '0.0.0.0'