export numRound = (num, decimals) =>
  Math.round((num + Number.EPSILON) * decimals) / decimals
  
export numRound2 = (num) => numRound num, 100
export numRound3 = (num) => numRound num, 1000

export calcCijena = ({ cijenaTipUkupno, cijena, povrsina }) =>
  if cijenaTipUkupno
    { ukupno: cijena, pometru: numRound3 cijena / povrsina }
  else
    { ukupno: cijena * povrsina, pometru: cijena }


export fillImgObjs = (o) =>
  fill = (img) =>
    should = typeof img is 'number' and isFinite img
    if should then { ...o.slike[img], indx: img } else img
  o.base.slika = fill o.base.slika
  for i in o.info
    if i.slika or i.slika is 0 then i.slika = fill i.slika
    if i.slike
      for slika, s in i.slike
        i.slike[s] = fill slika
  o