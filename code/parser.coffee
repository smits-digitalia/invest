noReg = (str) => str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&")
find  = (char, str) => str.search noReg char
split = (str, delim='\n') => str.split noReg delim
flush = (arr) => arr.filter (x) => x isnt ''

getImgs = (arr) =>
  path = "https://res.cloudinary.com/n-smits/image/upload/v1584428986/#{arr[0]}/"
  ordr = -1
  sort = ''
  locs = []
  splitImgs = (result, imgs) =>
    if imgs[0] is '#'
      sort = imgs[2..]
      locs.push name: sort, link: ordr+1
    else
      img = split imgs
      head = img[0].split ' '
      ordr = parseInt head[0]
      result.push index: ordr, file: "#{path}#{head[1]}", sort: sort, opis: img[1]
    result
  data: arr[1..].reduce splitImgs, []
  sort: locs
getBody = (arr, imgs) =>
  getImg = (i) => imgs[parseInt i]
  getList = (body, type, onListBody) =>
    arr = split body
    tip: type
    img: getImg arr[0]
    title: arr[1]
    data: arr[2..].map onListBody
  infoList = (body) => getList body, 'infoList', (str) => str[2..]
  infoStat = (body) => getList body, 'infoStat', (str) => str[2..].split ': '
  infoImgs = (body) =>
    if -1 is find '\n', body
      nums = body.split ' '
      tip: 'infoImgs'
      data: nums.map (i) => getImg i
    else false
  arr.map (body, i) =>
    if i is 0 then infoStat body
    else infoImgs(body) or infoList body

export parser = (file, path) =>
  if typeof file is 'object' then file
  else
    doc = file.split '---'
    ele = (i, delim='\n') => flush split doc[i], delim
    imgs = getImgs ele(1, '\n\n')
    body = getBody ele(0, '\n\n'), imgs.data
    base = {link: path, ...body[0]}
    base: base
    body: body
    imgs: imgs.data
    imgsort: imgs.sort